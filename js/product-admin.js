function openModal(id) {
    var element = document.getElementById(id);
    var background = document.getElementById("blurred-background");
    element.style.display = "flex";
    background.style.display = "block";
}

function closeModal(id) {
    var element = document.getElementById(id);
    var background = document.getElementById("blurred-background");
    element.style.display = "none";
    background.style.display = "none"
}