var isOpen = false;

function openModal() {
    var filter = document.getElementById("filterModal");
    if (!isOpen) {
        filter.style.display = "flex";
        isOpen = true;
    } else {
        filter.style.display = "none";
        isOpen = false;
    }
    
}

$( function() {
    $( "#slider-range-rating" ).slider({
    range: true,
    min: 1,
    max: 5,
    values: [ 1, 5 ],
    slide: function( event, ui ) {
        $( "#amount-rating" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
    }
    });
    $( "#amount-rating" ).val($( "#slider-range-rating" ).slider( "values", 0 ) +
    " - " + $( "#slider-range-rating" ).slider( "values", 1 ) );
} );

$( function() {
    $( "#slider-range-pricing" ).slider({
      range: true,
      min: 0,
      max: 2000,
      values: [ 75, 1500 ],
      slide: function( event, ui ) {
        $( "#amount-pricing" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount-pricing" ).val( "$" + $( "#slider-range-pricing" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range-pricing" ).slider( "values", 1 ) );
} );